# Soundboard
Lesson Task: 3.9 \
You are creating the cheap version of the pad controller. Source of inspiration:
https://www.youtube.com/watch?v=4ZjzGIlEwRs
* There are at least 16 pads. 
* Pressing any pad, plays the sound.
* Pads have different colours, they flash after pressed.
* (optional) Name of the sound is displayed. 
* (optional) Each sound is unique. 
* (optional) Add slider to control the volume of produced sound.

All tasks are implimented in addition to the ability to select the output device and making the window frameless.

## Contributors
Eivind Vold Aunebakk

## Screenshots
![screenshot](screenshots/screenshot.png)
