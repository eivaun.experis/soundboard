import QtQuick
import QtMultimedia
import QtQuick.Layouts
import QtQuick.Controls

ApplicationWindow {
    id: win
    width: 1280
    height: 720
    visible: true
    title: qsTr("Soundboard")
    flags: Qt.FramelessWindowHint | Qt.Window

    property int winState: 0
    onWindowStateChanged: function(state) {
        winState = state;
        print("onWindowStateChanged: " + state);
    }

    property var sounds: [
        "808 Bass A.WAV",
        "Bell 001 Revved.wav",
        "Down Rise Fx.wav",
        "Drop FX.wav",
        "Gun Shot.wav",
        "Kick 007 Noisy.wav",
        "Kick Drop.wav",
        "Loading a Gun.wav",
        "Money Counter.wav",
        "Noise Layer.wav",
        "Orchestra Hit 001 Revved.wav",
        "Rumble to Hiss.wav",
        "Timpani 001 Revved.wav",
        "Up.wav",
        "Zap 001.wav",
        "Zap 002.wav"
    ]

    MediaDevices {
        id: devices
    }
    property var outputDevice: devices.defaultAudioOutput

    function getColor(i) {
        let col = i % 4;
        let row = 4 - Math.floor(i / 4)

        return Qt.hsva(0.0 + 1*(col/4), 1, 0.6 + 0.3*(row/4), 1)
    }

    header: ToolBar {
        MouseArea {
            id: windowDragArea
            anchors.fill: parent

            property int lastX: 0
            property int lastY: 0

            onPositionChanged: {
                var mPos = mapToItem( null, mouseX, mouseY );
                mPos.x += win.x;
                mPos.y += win.y;

                if (lastX && lastY) {
                    win.x += mPos.x - lastX;
                    win.y += mPos.y - lastY;
                }

                lastX = mPos.x;
                lastY = mPos.y;
            }
            onClicked: {
                lastX = 0
                lastY = 0;
            }
        }

        Label {
            id: windowTitle
            text: win.title
            font.pixelSize: 20
            anchors.centerIn: headerRowLayout
        }

        RowLayout {
            id: headerRowLayout
            anchors.fill: parent
            spacing: 0

            Item { Layout.fillWidth: true }

            ToolButton {
                icon.source: "/icons/minimize"
                onClicked: { win.showMinimized() }
            }
            ToolButton {
                icon.source: "/icons/maximize"
                onClicked: {
                    winState === Qt.WindowMaximized ? win.showNormal() : win.showMaximized();
                }
            }
            ToolButton {
                icon.source: "/icons/close"
                onClicked: Qt.quit()
            }
        }
    }

    ColumnLayout {
        id: mainContent
        anchors.fill: parent
        anchors.margins: 20

        Grid {
            id: buttonGrid
            Layout.fillWidth: true
            Layout.fillHeight: true
            columns:4
            rows: 4
            spacing:10
            Repeater
            {
                model: sounds.length
                AbstractButton {
                    id: button
                    width: (parent.width - parent.spacing*(parent.columns-1)) / parent.columns
                    height: (parent.height - parent.spacing*(parent.rows-1)) / parent.rows
                    property color color: getColor(index); //Material.accent
                    property color colorNormal: color
                    property color colorHovered: Qt.lighter(colorNormal, 1.1)
                    property color colorPressed: Qt.lighter(colorNormal, 1.2)
                    background: Rectangle {
                        border.width: 0
                        radius: 10
                        color: parent.pressed ? colorPressed : ( parent.hovered ? colorHovered : colorNormal)
                    }
                    contentItem: Label {
                        text:sounds[index].replace(/\.[^/.]+$/, "")
                        font.pixelSize: 20
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment:  Text.AlignVCenter
                    }
                    MediaPlayer {
                        id: sound
                        source: "/sounds/" + sounds[index]
                        audioOutput: AudioOutput {
                            device: outputDevice
                            volume: volumeSlider.value
                        }
                        onPlaybackStateChanged: {
                            if (playbackState === MediaPlayer.StoppedState) {
                                anim.stop()
                                colorNormal = color
                            }
                        }
                    }
                    onClicked: {
                        sound.stop()
                        sound.play()
                        anim.restart()
                    }
                    SequentialAnimation on colorNormal {
                        id: anim
                        running: false
                        loops: Animation.Infinite
                        ColorAnimation { from: color; to: colorPressed; duration: 100 }
                        ColorAnimation { from: colorPressed; to: color; duration: 300 }
                    }
                }
            }
        }

        RowLayout {
            id: bottomToolBar
            spacing: 10
            Label { text: "Volume:" }
            Slider {
                id: volumeSlider
                orientation: Qt.Horizontal
                value: 0.5
            }
            Item { Layout.fillWidth: true }
            Label { text: "Output device:" }
            ComboBox {
                Layout.preferredWidth: 300
                id: outputSelect
                editable: false
                displayText: outputDevice.description
                model: ListModel {
                    Component.onCompleted: {
                        for (var i = 0; i < devices.audioOutputs.length; ++i) {
                            append(devices.audioOutputs[i]);
                        }
                    }
                }
                delegate: ItemDelegate {
                    text: model.description
                    width: outputSelect.width
                    MouseArea {
                        anchors.fill: parent
                        onClicked: { outputSelect.currentIndex = index; outputSelect.popup.close() }
                    }
                }
                onCurrentIndexChanged: {
                    outputDevice = devices.audioOutputs[currentIndex]
                }
            }
        }
    }


    //
    // The following allows resizing a frameless window
    //

    property int resizeAreaSize: 10

    // Sides
    MouseArea {
        id: windowResizeAreaLeft
        width: resizeAreaSize
        z: win.header.z + 1
        anchors.top: win.header.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: resizeAreaSize
        anchors.bottomMargin: resizeAreaSize
        cursorShape: Qt.SizeHorCursor
        acceptedButtons: Qt.LeftButton
        pressAndHoldInterval: 100
        onPressAndHold: {
            win.startSystemResize(Qt.LeftEdge)
        }
    }
    MouseArea {
        id: windowResizeAreaRight
        width: resizeAreaSize
        z: win.header.z + 1
        anchors.top: win.header.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.topMargin: resizeAreaSize
        anchors.bottomMargin: resizeAreaSize
        cursorShape: Qt.SizeHorCursor
        acceptedButtons: Qt.LeftButton
        pressAndHoldInterval: 100
        onPressAndHold: {
            win.startSystemResize(Qt.RightEdge)
        }
    }
    MouseArea {
        id: windowResizeAreaTop
        height: resizeAreaSize
        z: win.header.z + 1
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: win.header.top
        anchors.leftMargin: resizeAreaSize
        anchors.rightMargin: resizeAreaSize
        cursorShape: Qt.SizeVerCursor
        acceptedButtons: Qt.LeftButton
        pressAndHoldInterval: 100
        onPressAndHold: {
            win.startSystemResize(Qt.TopEdge)
        }
    }
    MouseArea {
        id: windowResizeAreaBottom
        height: resizeAreaSize
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.leftMargin: resizeAreaSize
        anchors.rightMargin: resizeAreaSize
        cursorShape: Qt.SizeVerCursor
        acceptedButtons: Qt.LeftButton
        pressAndHoldInterval: 100
        onPressAndHold: {
            win.startSystemResize(Qt.BottomEdge)
        }
    }

    // Corners
    MouseArea {
        id: windowResizeAreaTopRight
        width: resizeAreaSize
        height: resizeAreaSize
        z: win.header.z + 1
        anchors.top: win.header.top
        anchors.right: parent.right
        cursorShape: Qt.SizeBDiagCursor
        acceptedButtons: Qt.LeftButton
        pressAndHoldInterval: 100
        onPressAndHold: {
            win.startSystemResize(Qt.TopEdge | Qt.RightEdge)
        }
    }
    MouseArea {
        id: windowResizeAreaTopLeft
        width: resizeAreaSize
        height: resizeAreaSize
        z: win.header.z + 1
        anchors.top: win.header.top
        anchors.left: parent.left
        cursorShape: Qt.SizeFDiagCursor
        acceptedButtons: Qt.LeftButton
        pressAndHoldInterval: 100
        onPressAndHold: {
            win.startSystemResize(Qt.TopEdge | Qt.LeftEdge)
        }
    }
    MouseArea {
        id: windowResizeAreaBottomRight
        width: resizeAreaSize
        height: resizeAreaSize
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        cursorShape: Qt.SizeFDiagCursor
        acceptedButtons: Qt.LeftButton
        pressAndHoldInterval: 100
        onPressAndHold: {
            win.startSystemResize(Qt.BottomEdge | Qt.RightEdge)
        }
    }
    MouseArea {
        id: windowResizeAreaBottomLeft
        width: resizeAreaSize
        height: resizeAreaSize
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        cursorShape: Qt.SizeBDiagCursor
        acceptedButtons: Qt.LeftButton
        pressAndHoldInterval: 100
        onPressAndHold: {
            win.startSystemResize(Qt.BottomEdge | Qt.LeftEdge)
        }
    }
}
